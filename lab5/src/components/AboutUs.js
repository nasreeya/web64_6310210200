import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUs (porps) {


    return (
        <Box sx={{ width :"60%"}}>
            <Paper elevation={3}>
                <h2> จัดทำโดย: { porps.name }</h2>
                <h3> ติดต่อนัสรียาได้ที่ { porps.address }</h3>
                <h3> บ้านนัสรียา อยู่ที่ { porps.province } </h3>
            </Paper>
        </Box>
    );

}

export default AboutUs;
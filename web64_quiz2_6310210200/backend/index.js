const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const mysql = require("mysql")

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'runner_admin',
    password : 'runner_admin',
    database : 'BuffetSystem'
});

connection.connect()

const express = require('express');
const app = express()
const port = 4000

app.post("/add_buffet", (req, res) => {
    let registration_id = req.query.registration_id
    let seat_id = req.query.seat_id
    let registration_time = req.query.registration_time

    let query = `INSERT INTO Registration
                    (RegistrationID, SeatID, RegistrationTime)
                    VALUES ('${registration_id}', '${seat_id}', '${registration_time}')`
    console.log(query)

    connection.query(query, (err, rows) => {
        if(err) {
            res.json({
                "status" : "400",
                "message" : "Error inserting data into"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding buffet succesful"
            })
        }
    });
})

app.get("/list_buffet", (req, res) => {
    let query = "SELECT * FROM Registration"
    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error querying from Registration"
            })
        }else {
            res.json(rows)
        }
    });
})

app.post("/update_registration", (req, res) => {

    let registration_id = req.query.registration_id
    let seat_id = req.query.seat_id
    let registration_time = req.query.registration_time
    let customer_id = req.query.customer_id

    let query = `UPDATE Regitration SET 
                    RegistrationID = '${registration_id}',
                    SeatID = '${seat_id}',
                    RegistrationTime = '${registration_time}',
                    CustomerID = '${customer_id}'`

    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error updating record"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Updating succesful"
            })
        }
    });
})

app.post("/delete_registration", (req, res) => {

    let seat_id = req.query.seat_id

    let query = `DELETE FROM Regitration WHERE SeatID = ${seat_id}`

    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error deleting record"
            })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting succesful"
            })
        }
    });
})
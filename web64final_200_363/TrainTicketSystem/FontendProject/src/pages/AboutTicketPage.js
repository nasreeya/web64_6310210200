
import AboutUs from '../components/AboutTicket';
function AboutTicketPage() {


    return (
        <div>
            <div align="center">
                <h2> รายละเอียดผู้จอง </h2>

                <AboutUs name="นัสรียา กุลมาศ"
                        address="หาดใหญ่" 
                        province="กระบี่" />

            </div>
        </div>
    );
}

export default AboutTicketPage;

import BookingResult from "../components/BookingResult";
import { useState } from "react";

import Button from '@mui/material/Button';

function BookingTicketPage() {

    const [ name, setName ] = useState("");
    const [ surname, setSurname ] = useState("");
    const [ telephonenumber, setTelephoneNumber ] = useState("");

    const [ origin, setOrigin ] = useState("")
    const [ terminal, setTerminal ] = useState("")

    const [ time, setTime ] = useState("")

        ( 
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บจองตั๋วรถไฟ
                <hr />
                ชื่อ: <input type="text" 
                        value={ name }
                        onChange={ (e) => {setName(e.target.value); } } />  <br />
                สกุล: <input type="text" 
                        value={ surname }
                        onChange={ (e) => {setSurname(e.target.value); } } />  <br />
                เบอร์โทรศัพท์: <input type="text" 
                        value={ telephonenumber }
                        onChange={ (e) => {setTelephoneNumber(e.target.value); } } />  <br />  
                สถานีต้นทาง: <input type="text" 
                        value={ origin }
                        onChange={ (e) => {setOrigin(e.target.value); } } />  <br />
                สถานีปลายทาง: <input type="text" 
                        value={ terminal }
                        onChange={ (e) => {setTerminal(e.target.value); } } />  <br />
                เวลาที่จอง: <input type="text" 
                        value={ time }
                        onChange={ (e) => {setTime(e.target.value); } } />  <br />

                <Button variant="contained"  onClick={ ()=>{BookingTicketPage() } } >
                    จองตั๋ว
                </Button>

                {  BookingResult != 0 && 
                    <div>
                        <hr />
                        รายละเอียดการจองของคุณ
                        <BookingResult 
                            name={ name }
                            surname = { surname }
                            telephonenumber = {telephonenumber}
                            origin = { origin }
                            terminal = { terminal }
                            time = { time }

                        />
                    </div>
                }
            </div>
        </div>
    );
}
export default BookingTicketPage;

import './App.css';

import AboutUsPage from './pages/AboutTicketPage';
import BookingTicketPage from './pages/BookingTicketPage';
import Header from './components/Header';

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
            <Header />
          <Routes>
              <Route path="about" element={
                        <AboutUsPage />
                      } />

              <Route path="/" element={
                        <BookingTicketPage />
                      } />
          </Routes>
    </div>
  );
}

export default App;

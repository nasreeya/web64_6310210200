import React from 'react';
import ReactDOM from 'react=dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from "react-router-dom";

import { createTheme, ThemeProvider } from '@mui/material/styles';
import { green, purple } from '@mui/material/colors';

const theme = createTheme({
    palette: {
        type: 'light',
        primary: {
          main: '#4c061d',
          light: '#e281a1',
          dark: '#580923',
        },
        secondary: {
          main: '#11a236',
        },
        warning: {
          main: '#060260',
        },
        background: {
          paper: '#acf3ff',
          default: '#9ab0e6',
        },
        text: {
          primary: 'rgba(0,0,0,0.87)',
        },
        error: {
          main: '#f43636',
        },
      },
    };

ReactDOM.render(
    <BrowserRouter>
    <ThemeProvider theme={theme}>
        <App />
    </ThemeProvider>
    </BrowserRouter>,
    document.getElementById('root')
);




reportWebVitals();

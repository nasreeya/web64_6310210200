import {Link } from "react-router-dom";

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
            <Typography variant="h5" component="div">
              ยินดีต้อนรับสู่เว็บจองตั๋วรถไฟ
            </Typography>

            <Link to="/">
                <Typography variant="body1">
                  จองตั๋ว
                </Typography>
            </Link>
            &nbsp;&nbsp;&nbsp;

            <Link to="/about">
                <Typography variant="body1">
                    รายละเอียดการจองของคุณ
                </Typography>
        </Link>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header;

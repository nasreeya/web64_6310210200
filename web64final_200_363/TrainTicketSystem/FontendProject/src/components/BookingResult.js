
function BookingResult (props) {



    return (

        <div>
            <h3>ชื่อ: {props.name}</h3>
            <h3>สกุล: {props.surname}</h3>
            <h3>เบอร์โทรศัพท์: {props.telephone}</h3>
            <h3>สถานีต้นทาง: {props.origin}</h3>
            <h3>สถานีปลายทาง: {props.terminal}</h3>
            <h3>เวลา: {props.time}</h3>
            <h3></h3>
        </div>

    );
}

export default BookingResult;
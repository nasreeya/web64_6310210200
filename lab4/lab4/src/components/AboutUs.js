
function AboutUs (porps) {


    return (
        <div>
            <h2> จัดทำโดย: { porps.name }</h2>
            <h3> ติดต่อนัสรียาได้ที่ { porps.address }</h3>
            <h3> บ้านนัสรียา อยู่ที่ { porps.province } </h3>
        </div>
    );

}

export default AboutUs;